package com.tsc.skuschenko.tm.exception;

import org.jetbrains.annotations.Nullable;

public class AbstractException extends RuntimeException {

    public AbstractException(@Nullable final String value) {
        super(value);
    }

}
