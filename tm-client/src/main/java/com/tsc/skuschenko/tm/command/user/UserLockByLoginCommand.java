package com.tsc.skuschenko.tm.command.user;


import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class UserLockByLoginCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "lock user by login";

    @NotNull
    private static final String NAME = "lock-user-by-login";

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        showParameterInfo("login");
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getUserEndpoint().lockUserByLogin(session, login);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String[] roles() {
        return new String[]{"Administrator"};
    }

}
