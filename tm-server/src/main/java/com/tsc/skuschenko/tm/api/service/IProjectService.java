package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.dto.Project;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    @NotNull
    Project add(
            @Nullable String userId, @Nullable String name,
            @Nullable String description
    );

    void addAll(@Nullable List<Project> projects);

    @Nullable
    List<Project> findAll();

    @SneakyThrows
    void clear();

    @NotNull
    Project changeStatusById(
            @NotNull String userId, @Nullable String id,
            @Nullable Status status
    );

    @NotNull
    Project changeStatusByIndex(
            @NotNull String userId, @Nullable Integer index,
            @Nullable Status status
    );

    @NotNull
    Project changeStatusByName(
            @NotNull String userId, @Nullable String name,
            @Nullable Status status
    );

    void clear(@NotNull String userId);

    @NotNull
    Project completeById(@NotNull String userId, @Nullable String id);

    @NotNull
    Project completeByIndex(@NotNull String userId, @Nullable Integer index);

    @NotNull
    Project completeByName(@NotNull String userId, @Nullable String name);

    @NotNull
    List<Project> findAll(
            @NotNull String userId,
             @Nullable Comparator<Project> comparator
    ) ;

    @Nullable
    List<Project> findAll(@NotNull String userId);

    @Nullable
    Project findOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    Project findOneByIndex(@NotNull String userId, Integer index);

    @Nullable
    Project findOneByName(@NotNull String userId, String name);

    @Nullable
    Project removeOneById(@NotNull String userId, String id);

    @Nullable
    Project removeOneByIndex(@NotNull String userId, Integer index);

    @Nullable
    Project removeOneByName(@NotNull String userId, @Nullable String name);

    @NotNull
    Project startById(@NotNull String userId, @Nullable String id);

    @NotNull
    Project startByIndex(@NotNull String userId, @Nullable Integer index);

    @NotNull
    Project startByName(
            @NotNull String userId, @Nullable String name
    );

    @NotNull
    Project updateOneById(
            @NotNull String userId, @Nullable String id,
            @Nullable String name, @Nullable String description
    );

    @NotNull
    Project updateOneByIndex(
            @NotNull String userId, @Nullable Integer index,
            @Nullable String name, @Nullable String description
    );

}
