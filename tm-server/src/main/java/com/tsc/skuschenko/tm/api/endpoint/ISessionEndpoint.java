package com.tsc.skuschenko.tm.api.endpoint;

import com.tsc.skuschenko.tm.dto.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ISessionEndpoint {

    void closeSession(@NotNull Session session);

    @Nullable
    Session openSession(@NotNull String login, @NotNull String password);

}
