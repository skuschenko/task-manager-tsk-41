package com.tsc.skuschenko.tm.exception.system;

import com.tsc.skuschenko.tm.exception.AbstractException;
import org.jetbrains.annotations.Nullable;

public final class ConnectionFailedException extends AbstractException {

    public ConnectionFailedException() {
        super("Error! Connection is failed...");
    }

    public ConnectionFailedException(@Nullable final String param) {

        super("Error! " + param + " is empty...");
    }

}
