package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.api.repository.ISessionRepository;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.dto.Session;
import com.tsc.skuschenko.tm.dto.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ISessionService {

    @Nullable
    User checkDataAccess(@Nullable String login, @Nullable String password);

    void close(@NotNull Session session) throws AccessForbiddenException;

    ISessionRepository getSessionRepository();

    @Nullable
    Session open(@NotNull String login, @NotNull String password);

    @Nullable
    Session sign(@Nullable Session session);

    void validate(@Nullable Session session, @Nullable Role role)
            throws AccessForbiddenException;

    void validate(@Nullable Session session) throws AccessForbiddenException;

}
