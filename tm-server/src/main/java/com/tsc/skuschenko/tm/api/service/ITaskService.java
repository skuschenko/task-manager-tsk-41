package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.dto.Task;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface ITaskService{

    @NotNull
    Task add(
            @Nullable String userId, @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task changeStatusById(
            @NotNull String userId, @Nullable String id,
            @Nullable Status status
    );

    @NotNull
    Task changeStatusByIndex(
            @NotNull String userId, @Nullable Integer index,
            @Nullable Status status
    );

    @NotNull
    Task changeStatusByName(
            @NotNull String userId, @Nullable String name,
            @Nullable Status status
    );

    void clear(@NotNull String userId);

    @NotNull
    Task completeById(
            @NotNull String userId, @Nullable String id
    );

    @NotNull
    Task completeByIndex(
            @NotNull String userId, @Nullable Integer index
    );

    @NotNull
    Task completeByName(@NotNull String userId,
                        @Nullable String name);

    @NotNull
    List<Task> findAll(
            @NotNull String userId,
            @Nullable Comparator<Task> comparator
    );

    void addAll(@Nullable List<Task> tasks);

    @Nullable
    List<Task> findAll(@NotNull String userId);

    @Nullable
    List<Task> findAll();

    @SneakyThrows
    void clear();

    @Nullable
    Task findOneById(
            @NotNull String userId, @Nullable String id
    );

    @Nullable
    Task findOneByIndex(
            @NotNull String userId, Integer index
    );

    @Nullable
    Task findOneByName(
            @NotNull String userId, String name
    );

    @Nullable
    Task removeOneById(
            @NotNull String userId, String id
    );

    @Nullable
    Task removeOneByIndex(
            @NotNull String userId, Integer index
    );

    @Nullable
    Task removeOneByName(
            @NotNull String userId, @Nullable String name
    );

    @NotNull
    Task startById(
            @NotNull String userId, @Nullable String id
    );

    @NotNull
    Task startByIndex(
            @NotNull String userId, @Nullable Integer index
    );

    @NotNull
    Task startByName(
            @NotNull String userId, @Nullable String name
    );

    @NotNull
    Task updateOneById(
            @NotNull String userId, @Nullable String id,
            @Nullable String name, @Nullable String description
    );

    @NotNull
    Task updateOneByIndex(
            @NotNull String userId, @Nullable Integer index,
            @Nullable String name, @Nullable String description
    );

}
