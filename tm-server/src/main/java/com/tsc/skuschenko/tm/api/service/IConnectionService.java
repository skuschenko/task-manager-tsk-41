package com.tsc.skuschenko.tm.api.service;


import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;

public interface IConnectionService {

    @NotNull Connection getConnection();

    @NotNull SqlSession getSqlConnection();

    @NotNull SqlSessionFactory getSqlSessionFactory();

}
