package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.dto.Task;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO tm_task" +
            "(id, dateBegin, dateEnd, description, name, user_id, status, " +
            "created,project_id) VALUES(#{id},#{dateStart},#{dateFinish}," +
            "#{description}, #{name},#{userId},#{status},#{created}," +
            "#{projectId})")
    void add(@NotNull Task task);

    @Delete("DELETE FROM tm_task WHERE user_id =#{userId}")
    void clear(@NotNull String userId);

    @Delete("DELETE FROM tm_task")
    void clearAllTasks();

    @Select("SELECT * FROM tm_task")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "projectId", column = "project_id"),
    })
    @Nullable List<Task> findAll();

    @Select("SELECT * FROM tm_task WHERE project_id=#{projectId}" +
            "AND user_id=#{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "project_id", column = "projectId"),
    })
    @Nullable
    List<Task> findAllTaskByProjectId(
            @NotNull String userId, @NotNull String projectId
    );

    @Select("SELECT * FROM tm_task WHERE user_id=#{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "projectId", column = "project_id"),
    })
    @Nullable List<Task> findAllWithUserId(@NotNull String userId);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE id=#{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "projectId", column = "project_id"),
    })
    Task findById(@NotNull String id);

    @Select("SELECT * FROM tm_task WHERE WHERE id = #{id} AND " +
            "user_id=#{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "projectId", column = "project_id"),
    })
    @Nullable
    Task findOneById(@NotNull String userId, @NotNull String id);

    @Select("SELECT * FROM tm_task WHERE WHERE AND " +
            "user_id=#{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "projectId", column = "project_id"),
    })
    @Nullable
    Task findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Select("SELECT * FROM tm_task WHERE WHERE name = #{name} AND " +
            "user_id=#{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "projectId", column = "project_id"),
    })
    @Nullable
    Task findOneByName(@NotNull String userId, @NotNull String name);

    @Delete("DELETE FROM tm_task WHERE id={id}")
    void remove(@NotNull Task task);

    @Delete("DELETE FROM tm_task WHERE user_id  =#{userId }" +
            "AND id=#{taskId}")
    void removeOneById(@NotNull String userId, @NotNull String taskId);

    @Delete("DELETE FROM tm_task WHERE user_id  =#{userId }")
    void removeTaskQuery(@NotNull Task task);

    @Update("UPDATE tm_task SET id=#{id}, dateBegin=#{dateStart}, " +
            "dateEnd=#{dateFinish}, description=#{description}," +
            " name=#{name}, user_id=#{userId}, status=#{status}, " +
            "created=#{created},project_id=#{projectId}")
    void updateTaskQuery(@NotNull Task task);

}
