package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.dto.Session;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session" +
            "(id, timestamp, signature, user_id) " +
            "VALUES(#{id},#{timestamp},#{signature},#{user_id})")
    void add(@NotNull Session session);

    @Select("SELECT * FROM tm_session WHERE user_id=#{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "signature", column = "signature"),
            @Result(property = "timestamp", column = "timestamp"),
            @Result(property = "signature", column = "signature"),
            @Result(property = "userId", column = "user_id"),
    })
    @Nullable List<Session> findAll(@NotNull String userId);

    @Nullable
    @Select("SELECT FROM tm_session WHERE id=#{id }")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "signature", column = "signature"),
            @Result(property = "timestamp", column = "timestamp"),
            @Result(property = "signature", column = "signature"),
            @Result(property = "userId", column = "user_id"),
    })
    Session findSessionById(Session session);

    @Delete("DELETE FROM tm_session WHERE id=#{id }")
    void removeSessionById(Session session);

    @Delete("DELETE FROM tm_session WHERE user_id=#{user_id }")
    void removeSessionQuery(Session session);

    @Update("UPDATE tm_session SET id=#{id}, timestamp=#{timestamp}, " +
            "signature=#{signature}, user_id=#{userId}")
    void updateSessionQuery(Session session);

}
