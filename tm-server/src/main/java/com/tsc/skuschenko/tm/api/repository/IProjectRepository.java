package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.dto.Project;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project" +
            "(id, dateBegin, dateEnd, description, name, user_id, status, " +
            "created) VALUES(#{id},#{dateStart},#{dateFinish},#{description}," +
            "#{name},#{userId},#{status},#{created})")
    void add(@NotNull Project project);

    @Delete("DELETE FROM tm_project WHERE user_id =#{userId}")
    void clear(@NotNull String userId);

    @Delete("DELETE FROM tm_project")
    void clearAllProjects();

    @Select("SELECT * FROM tm_project")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
    })
    @Nullable List<Project> findAll();

    @Select("SELECT * FROM tm_project WHERE user_id=#{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
    })
    @Nullable List<Project> findAllWithUserId(@NotNull String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE id=#{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
    })
    Project findById(@NotNull String id);

    @Select("SELECT * FROM tm_project WHERE WHERE id = #{id} AND " +
            "user_id=#{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
    })
    @Nullable
    Project findOneById(@NotNull String userId, @NotNull String id);

    @Select("SELECT * FROM tm_project WHERE WHERE AND " +
            "user_id=#{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
    })
    @Nullable
    Project findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Select("SELECT * FROM tm_project WHERE WHERE name = #{name} AND " +
            "user_id=#{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
    })
    @Nullable
    Project findOneByName(@NotNull String userId, @NotNull String name);

    @Delete("DELETE FROM tm_project WHERE id={id}")
    void remove(@NotNull Project project);

    @Delete("DELETE FROM tm_project WHERE user_id  =#{userId }" +
            "AND id=#{projectId}")
    void removeOneById(@NotNull String userId, @NotNull String projectId);

    @Delete("DELETE FROM tm_project WHERE user_id  =#{userId }")
    void removeProjectQuery(@NotNull Project project);

    @Update("UPDATE tm_project SET id=#{id}, dateBegin=#{dateStart}, " +
            "dateEnd=#{dateFinish}, description=#{description}, name=#{name}, " +
            "userId=#{user_id}, status=#{status}, created=#{created}")
    void updateProjectQuery(@NotNull Project project);

}