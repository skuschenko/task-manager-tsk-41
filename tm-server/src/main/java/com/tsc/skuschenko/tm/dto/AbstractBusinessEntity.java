package com.tsc.skuschenko.tm.dto;

import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.entity.user.AccessDeniedException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public class AbstractBusinessEntity extends AbstractEntity {

    @Column
    @Nullable
    private Date created = new Date();

    @Column(name = "dateEnd")
    @Nullable
    private Date dateFinish;

    @Column(name = "dateBegin")
    @Nullable
    private Date dateStart;

    @Column
    @Nullable
    private String description = "";

    @Column
    @Nullable
    private String name = "";

    @Column
    @Nullable
    private String status = Status.NOT_STARTED.getDisplayName();

    @Column(name = "user_id")
    @Nullable
    private String userId;

    public String getUserId() {
        if (userId == null || userId.isEmpty()) {
            throw new AccessDeniedException();
        }
        return userId;
    }

    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}
