package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.dto.Project;
import com.tsc.skuschenko.tm.dto.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectTaskService {

    @NotNull
    Task bindTaskByProject(
            @NotNull String userId, @Nullable String projectId,
            @Nullable String taskId);

    void clearProjects(@NotNull String userId);

    @Nullable
    Project deleteProjectById(
            @NotNull String userId, @Nullable String projectId
    );

    @Nullable
    List<Task> findAllTaskByProjectId(
            @NotNull String userId, @Nullable String projectId
    );

    @NotNull
    Task unbindTaskFromProject(
            @NotNull String userId, @Nullable String projectId,
            @Nullable String taskId
    );

}
