package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.repository.IUserRepository;
import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.api.service.IUserService;
import com.tsc.skuschenko.tm.dto.User;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.empty.*;
import com.tsc.skuschenko.tm.exception.entity.user.EmailExistsException;
import com.tsc.skuschenko.tm.exception.entity.user.LoginExistsException;
import com.tsc.skuschenko.tm.exception.entity.user.UserNotFoundException;
import com.tsc.skuschenko.tm.util.HashUtil;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

public final class UserService implements IUserService {

    @NotNull
    private final IConnectionService connectionService;
    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IPropertyService propertyService
    ) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @Override
    public void addAll(@Nullable final List<User> users) {
        users.forEach(item ->
                create(item.getLogin(), item.getPasswordHash(), item.getEmail())
        );
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        try {
            @NotNull final IUserRepository userRepository =
                    sqlSession.getMapper(IUserRepository.class);
            userRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login, @Nullable final String password
    ) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        if (isLoginExist(login)) throw new LoginExistsException();
        @Nullable final String secret = propertyService.getPasswordSecret();
        @Nullable final Integer iteration
                = propertyService.getPasswordIteration();
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IUserRepository userRepository =
                sqlSession.getMapper(IUserRepository.class);
        try {
            @NotNull final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(secret, iteration, password));
            userRepository.add(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login, @Nullable final String password,
            @Nullable final String email
    ) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
        if (isEmailExist(email)) throw new EmailExistsException();
        if (isLoginExist(login)) throw new LoginExistsException();
        @Nullable final String secret = propertyService.getPasswordSecret();
        @Nullable final Integer iteration
                = propertyService.getPasswordIteration();
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IUserRepository userRepository =
                sqlSession.getMapper(IUserRepository.class);
        try {
            @NotNull final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(secret, iteration, password));
            user.setEmail(email);
            userRepository.add(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login, @Nullable final String password,
            @Nullable final Role role
    ) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(role).orElseThrow(EmptyRoleException::new);
        if (isLoginExist(login)) throw new LoginExistsException();
        @Nullable final String secret = propertyService.getPasswordSecret();
        @Nullable final Integer iteration
                = propertyService.getPasswordIteration();
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IUserRepository userRepository =
                sqlSession.getMapper(IUserRepository.class);
        try {
            @NotNull final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(secret, iteration, password));
            user.setEmail(role.getDisplayName());
            userRepository.add(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<User> findAll() {
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IUserRepository entityRepository =
                sqlSession.getMapper(IUserRepository.class);
        try {
            return entityRepository.findAll();
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyEmailException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IUserRepository userRepository =
                sqlSession.getMapper(IUserRepository.class);
        try {
            return userRepository.findById(id);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findById(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IUserRepository userRepository =
                sqlSession.getMapper(IUserRepository.class);
        try {
            return userRepository.findByEmail(email);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IUserRepository userRepository =
                sqlSession.getMapper(IUserRepository.class);
        try {
            return userRepository.findByLogin(login);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) {
        if (!Optional.ofNullable(email).isPresent()) return false;
        return findByEmail(email) != null;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (!Optional.ofNullable(login).isPresent()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IUserRepository userRepository =
                sqlSession.getMapper(IUserRepository.class);
        try {
            @NotNull final User user = Optional.ofNullable(findByLogin(login))
                    .orElseThrow(UserNotFoundException::new);
            user.setLocked(true);
            userRepository.updateUserQuery(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IUserRepository userRepository =
                sqlSession.getMapper(IUserRepository.class);
        try {
            return userRepository.removeByLogin(login);
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(
            @Nullable final String userId, @Nullable final String password
    ) {
        Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        @Nullable final String secret = propertyService.getPasswordSecret();
        @Nullable final Integer iteration
                = propertyService.getPasswordIteration();
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IUserRepository userRepository =
                sqlSession.getMapper(IUserRepository.class);
        try {
            @NotNull final User user = Optional.ofNullable(findById(userId))
                    .orElseThrow(UserNotFoundException::new);
            user.setPasswordHash(HashUtil.salt(secret, iteration, password));
            userRepository.updateUserQuery(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IUserRepository userRepository =
                sqlSession.getMapper(IUserRepository.class);
        try {
            @NotNull final User user = Optional.ofNullable(findByLogin(login))
                    .orElseThrow(UserNotFoundException::new);
            user.setLocked(false);
            userRepository.updateUserQuery(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(
            @Nullable final String userId, @Nullable final String firstName,
            @Nullable final String lastName, @Nullable final String middleName
    ) {
        Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IUserRepository userRepository =
                sqlSession.getMapper(IUserRepository.class);
        try {
            @NotNull final User user = Optional.ofNullable(findById(userId))
                    .orElseThrow(UserNotFoundException::new);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            userRepository.updateUserQuery(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}