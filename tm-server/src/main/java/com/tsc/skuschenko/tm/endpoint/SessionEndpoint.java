package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.endpoint.ISessionEndpoint;
import com.tsc.skuschenko.tm.api.service.IServiceLocator;
import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.dto.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @Nullable
    private ISessionService sessionService;

    public SessionEndpoint() {
        super(null);
    }

    public SessionEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.sessionService = serviceLocator.getSessionService();
    }

    @WebMethod
    @Override
    public void closeSession(
            @WebParam(name = "session", partName = "session")
            @NotNull Session session
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().close(session);
    }

    @WebMethod
    @Nullable
    @Override
    public Session openSession(
            @WebParam(name = "login", partName = "login")
            @NotNull final String login,
            @WebParam(name = "password", partName = "password")
            @NotNull final String password
    ) {
        return serviceLocator.getSessionService().open(login, password);
    }

}
