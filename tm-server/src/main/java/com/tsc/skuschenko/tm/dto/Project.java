package com.tsc.skuschenko.tm.dto;

import com.tsc.skuschenko.tm.api.entity.IWBS;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
public final class Project extends AbstractBusinessEntity implements IWBS {

}
