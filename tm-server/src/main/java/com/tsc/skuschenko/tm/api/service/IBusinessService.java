package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.api.IService;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.dto.AbstractBusinessEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface IBusinessService<E extends AbstractBusinessEntity>
        extends IService<E> {

    @NotNull
    E changeStatusById(
            @NotNull String userId, @Nullable String id, @Nullable Status status
    );

    @NotNull
    E changeStatusByIndex(
            @NotNull String userId, @Nullable Integer index,
            @Nullable Status status
    );

    @NotNull
    E changeStatusByName(
            @NotNull String userId, @Nullable String name,
            @Nullable Status status
    );

    void clear(String userId);

    @NotNull
    E completeById(@NotNull String userId, @Nullable String id);

    @NotNull
    E completeByIndex(@NotNull String userId, @Nullable Integer index);

    @NotNull
    E completeByName(@NotNull String userId, @Nullable String name);

    @Nullable
    List<E> findAll(@NotNull String userId, @Nullable Comparator<E> comparator);

    @Nullable
    List<E> findAll(@NotNull String userId);

    @Nullable
    E findOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    E findOneByIndex(@NotNull String userId, @Nullable Integer index);

    @Nullable
    E findOneByName(@NotNull String userId, @Nullable String name);

    @Nullable
    E removeOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    E removeOneByIndex(@NotNull String userId, @Nullable Integer index);

    @Nullable
    E removeOneByName(@NotNull String userId, @Nullable String name);

    @NotNull
    E startById(@NotNull String userId, @Nullable String id);

    @NotNull
    E startByIndex(@NotNull String userId, @Nullable Integer index);

    @NotNull
    E startByName(@NotNull String userId, @Nullable String name);

    @NotNull
    E updateOneById(
            @NotNull String userId, @Nullable String id,
            @Nullable String name, @Nullable String description
    );

    @NotNull
    E updateOneByIndex(
            @NotNull String userId, @Nullable Integer index,
            @Nullable String name, @Nullable String description
    );

}
