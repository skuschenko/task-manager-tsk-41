package com.tsc.skuschenko.tm.service;


import com.tsc.skuschenko.tm.api.repository.IProjectRepository;
import com.tsc.skuschenko.tm.api.repository.ISessionRepository;
import com.tsc.skuschenko.tm.api.repository.ITaskRepository;
import com.tsc.skuschenko.tm.api.repository.IUserRepository;
import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.dto.Project;
import com.tsc.skuschenko.tm.dto.Session;
import com.tsc.skuschenko.tm.dto.Task;
import com.tsc.skuschenko.tm.dto.User;
import com.tsc.skuschenko.tm.exception.system.ConnectionFailedException;
import lombok.SneakyThrows;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final EntityManagerFactory entityManagerFactory;
    @NotNull
    private final IPropertyService propertyService;
    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    public ConnectionService(@NotNull IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.sqlSessionFactory = getSqlSessionFactory();
        this.entityManagerFactory = getHibernateSqlFactory();
    }

    @NotNull
    @SneakyThrows
    @Override
    public Connection getConnection() {
        @NotNull final Map<String, String> properties = getConnectionProperty();
        @NotNull Connection connection =
                DriverManager.getConnection(
                        properties.get("url"),
                        properties.get("userName"),
                        properties.get("password")
                );
        connection.setAutoCommit(false);
        return connection;
    }

    @NotNull
    private Map<String, String> getConnectionProperty() {
        @NotNull final Map<String, String> properties =
                new HashMap<String, String>();
        @Nullable final String driver = propertyService.getJdbcDriver();
        Optional.ofNullable(driver).orElseThrow(() ->
                new ConnectionFailedException("Driver")
        );
        @Nullable final String userName = propertyService.getJdbcUserName();
        Optional.ofNullable(userName).orElseThrow(() ->
                new ConnectionFailedException("UserName")
        );
        @Nullable final String password = propertyService.getJdbcPassword();
        Optional.ofNullable(password).orElseThrow(() ->
                new ConnectionFailedException("Password")
        );
        @Nullable final String url = propertyService.getJdbcUrl();
        Optional.ofNullable(url).orElseThrow(() ->
                new ConnectionFailedException("Url")
        );
        @Nullable final String dialect = propertyService.getJdbcDialect();
        Optional.ofNullable(url).orElseThrow(() ->
                new ConnectionFailedException("Dialect")
        );
        @Nullable final String hbm2ddl = propertyService.getJdbcHbm2ddl();
        Optional.ofNullable(url).orElseThrow(() ->
                new ConnectionFailedException("Hbm2ddl")
        );
        @Nullable final String showSql = propertyService.getJdbcShowSql();
        Optional.ofNullable(url).orElseThrow(() ->
                new ConnectionFailedException("ShowSql")
        );
        properties.put("driver", driver);
        properties.put("userName", userName);
        properties.put("password", password);
        properties.put("url", url);
        properties.put("hbm2ddl", hbm2ddl);
        properties.put("dialect", dialect);
        properties.put("showSql", showSql);
        return properties;
    }

    @NotNull
    private EntityManagerFactory getHibernateSqlFactory() {
        @NotNull final Map<String, String> properties = getConnectionProperty();
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(
                org.hibernate.cfg.Environment.DRIVER, properties.get("driver")
        );
        settings.put(
                org.hibernate.cfg.Environment.URL, properties.get("url"))
        ;
        settings.put(
                org.hibernate.cfg.Environment.USER, properties.get("userName")
        );
        settings.put(
                org.hibernate.cfg.Environment.PASS, properties.get("password")
        );
        settings.put(
                org.hibernate.cfg.Environment.DIALECT,
                properties.get("dialect")
        );
        settings.put(
                org.hibernate.cfg.Environment.HBM2DDL_AUTO,
                properties.get("hbm2ddl")
        );
        settings.put(
                org.hibernate.cfg.Environment.SHOW_SQL,
                properties.get("showSql")
        );
        @NotNull final StandardServiceRegistryBuilder registryBuilder =
                new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry =
                registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(User.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Override
    @NotNull
    public SqlSession getSqlConnection() {
        return sqlSessionFactory.openSession();
    }

    @Override
    @NotNull
    public SqlSessionFactory getSqlSessionFactory() {
        @NotNull final Map<String, String> properties = getConnectionProperty();
        @NotNull final DataSource dataSource = new PooledDataSource(
                properties.get("driver"),
                properties.get("url"),
                properties.get("userName"),
                properties.get("password")
        );
        @NotNull final TransactionFactory transactionFactory =
                new JdbcTransactionFactory();
        @NotNull final Environment environment =
                new Environment("development", transactionFactory, dataSource);
        @NotNull final Configuration configuration =
                new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }


}
