package com.tsc.skuschenko.tm.dto;

import com.tsc.skuschenko.tm.enumerated.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Setter
@Entity
@NoArgsConstructor
public final class User extends AbstractEntity {

    @Column
    @Nullable
    private String email;

    @Column
    @Nullable
    private String firstName;

    @Column(name = "locked")
    private boolean isLocked = false;

    @Column
    @Nullable
    private String lastName;

    @Column
    @Nullable
    private String login;

    @Column
    @Nullable
    private String middleName;

    @Column
    @Nullable
    private String passwordHash;

    @Column
    @Nullable
    private String role = Role.USER.getDisplayName();

}
