package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.bootstrap.Bootstrap;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.dto.Session;
import com.tsc.skuschenko.tm.util.SignatureUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

public class SessionServiceTest {

    @Test
    public void close() {
        @NotNull ISessionService sessionService = testService();
        @Nullable final Session session = sessionService.open("user1", "user1");
        Assert.assertNotNull(session);
        sessionService.close(session);
        @Nullable final Session sessionFind =
                sessionService.getSessionRepository().findSessionById(session);
        sessionService.close(session);
        Assert.assertNull(sessionFind);
    }

    @Test
    public void open() {
        @NotNull ISessionService sessionService = testService();
        @NotNull final Session session = sessionService.open("user1", "user1");
        Assert.assertNotNull(session);
        @Nullable final Session sessionFind =
                sessionService.getSessionRepository().findSessionById(session);
        Assert.assertNotNull(sessionFind);
    }

    @Test(expected = AccessForbiddenException.class)
    public void openAccessForbidden() {
        @NotNull ISessionService sessionService = testService();
        @NotNull final Session session = sessionService.open("user2", "user2");
        Assert.assertNotNull(session);
        @Nullable final Session sessionFind =
                sessionService.getSessionRepository().findSessionById(session);
        Assert.assertNotNull(sessionFind);
    }

    @Test
    public void sign() {
        @NotNull ISessionService sessionService = testService();
        @NotNull final Session session = sessionService.open("user1", "user1");
        @Nullable final String signature =
                SignatureUtil.sign(session, "salt", 2565);
        session.setSignature(signature);
        @Nullable final Session sessionFind =
                sessionService.getSessionRepository().findSessionById(session);
        Assert.assertNotNull(sessionFind);
        Assert.assertEquals(signature, session.getSignature());
    }

    @NotNull
    private ISessionService testService() {
        @NotNull final IPropertyService propertyService =
                new PropertyService();
        @NotNull final IConnectionService connectionService =
                new ConnectionService(propertyService);
        Assert.assertNotNull(connectionService);
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        Assert.assertNotNull(bootstrap);
        bootstrap.getUserService().create("user1", "user1");
        @NotNull ISessionService sessionService =
                new SessionService(bootstrap, connectionService);
        Assert.assertNotNull(sessionService);
        return sessionService;
    }

}
