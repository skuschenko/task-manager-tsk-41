package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.IProjectRepository;
import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.dto.Project;
import com.tsc.skuschenko.tm.service.ConnectionService;
import com.tsc.skuschenko.tm.service.PropertyService;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

public class ProjectRepositoryTest {

    @NotNull
    final IPropertyService propertyService =
            new PropertyService();
    @NotNull
    final IConnectionService connectionService =
            new ConnectionService(propertyService);
    @NotNull
    final SqlSession sqlSession =
            connectionService.getSqlConnection();

    @Test
    public void testChangeStatusById() {
        @NotNull final Project project = testProjectModel();
        @NotNull final IProjectRepository
                projectRepository = testRepository(project);
        project.setStatus("status1");
        projectRepository.updateProjectQuery(project);
        sqlSession.commit();
        Assert.assertNotNull(project.getStatus());
        @Nullable final Project projectFind =
                projectRepository.findById(project.getId());
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals("status1", projectFind.getStatus());
    }

    @Test
    public void testChangeStatusByName() {
        @Nullable final Project project = testProjectModel();
        @NotNull final IProjectRepository
                projectRepository = testRepository(project);
        project.setStatus("status1");
        projectRepository.updateProjectQuery(project);
        sqlSession.commit();
        Assert.assertNotNull(project.getStatus());
        @Nullable final Project projectFind =
                projectRepository.findOneByName(
                        project.getUserId(), project.getName()
                );
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals("status1", projectFind.getStatus());
    }

    @Test
    public void testClear() {
        @Nullable final Project project = testProjectModel();
        @NotNull final IProjectRepository
                projectRepository = testRepository(project);
        projectRepository.clearAllProjects();
        sqlSession.commit();
        @Nullable final Project projectFind =
                projectRepository.findById(project.getId());
        Assert.assertNull(projectFind);
    }

    @Test
    public void testCompleteById() {
        @NotNull final Project project = testProjectModel();
        @NotNull final IProjectRepository
                projectRepository = testRepository(project);
        project.setStatus(Status.COMPLETE.getDisplayName());
        projectRepository.updateProjectQuery(project);
        sqlSession.commit();
        Assert.assertNotNull(project.getStatus());
        @Nullable final Project projectFind =
                projectRepository.findById(project.getId());
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testCompleteByIndex() {
        @NotNull final Project project = testProjectModel();
        @NotNull final IProjectRepository
                projectRepository = testRepository(project);
        project.setStatus(Status.COMPLETE.getDisplayName());
        projectRepository.updateProjectQuery(project);
        sqlSession.commit();
        Assert.assertNotNull(project.getStatus());
        @Nullable final Project projectFind =
                projectRepository.findOneByIndex(project.getUserId(), 0);
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testCompleteByName() {
        @NotNull final Project project = testProjectModel();
        project.setStatus(Status.COMPLETE.getDisplayName());
        @NotNull final IProjectRepository
                projectRepository = testRepository(project);
        projectRepository.updateProjectQuery(project);
        sqlSession.commit();
        Assert.assertNotNull(project.getName());
        @Nullable final Project projectFind =
                projectRepository.findOneByName(
                        project.getUserId(), project.getName()
                );
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testCreate() {
        @NotNull final Project project = testProjectModel();
        testRepository(project);
    }


    @Test
    public void testFindOneById() {
        @Nullable final Project project = testProjectModel();
        @NotNull final IProjectRepository
                projectRepository = testRepository(project);
        @Nullable final Project projectFind =
                projectRepository.findById(project.getId());
        Assert.assertNotNull(projectFind);
    }

    @Test
    public void testFindOneByIndex() {
        @Nullable final Project project = testProjectModel();
        @NotNull final IProjectRepository
                projectRepository = testRepository(project);
        @Nullable final Project projectFind =
                projectRepository.findOneByIndex(project.getUserId(), 0);
        Assert.assertNotNull(projectFind);
    }

    @Test
    public void testFindOneByName() {
        @Nullable final Project project = testProjectModel();
        @NotNull final IProjectRepository
                projectRepository = testRepository(project);
        @Nullable final Project projectFind =
                projectRepository.findOneByName(
                        project.getUserId(), project.getName()
                );
        Assert.assertNotNull(projectFind);
    }

    @NotNull
    private Project testProjectModel() {
        @Nullable final Project project = new Project();
        project.setUserId("72729b26-01dd-4314-8d8c-40fb8577c6b5");
        project.setName("name1");
        project.setDescription("des1");
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getUserId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("name1", project.getName());
        return project;
    }

    @Test
    public void testRemoveOneById() {
        @Nullable final Project project = testProjectModel();
        @NotNull final IProjectRepository
                projectRepository = testRepository(project);
        projectRepository.removeOneById(project.getUserId(), project.getId());
        sqlSession.commit();
        @Nullable final Project projectFind =
                projectRepository.findById(project.getId());
        Assert.assertNull(projectFind);
    }

    @NotNull
    private IProjectRepository testRepository(@NotNull final Project project) {

        @NotNull final IProjectRepository projectRepository =
                sqlSession.getMapper(IProjectRepository.class);
        Assert.assertTrue(
                projectRepository.findAllWithUserId(project.getUserId()).isEmpty()
        );
        projectRepository.add(project);
        sqlSession.commit();
        @Nullable final Project projectById =
                projectRepository.findById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(projectById.getId(), project.getId());
        return projectRepository;
    }

    @Test
    public void testStartById() {
        @NotNull final Project project = testProjectModel();
        @NotNull final IProjectRepository
                projectRepository = testRepository(project);
        project.setStatus(Status.IN_PROGRESS.getDisplayName());
        projectRepository.updateProjectQuery(project);
        sqlSession.commit();
        Assert.assertNotNull(project.getStatus());
        @Nullable final Project projectFind =
                projectRepository.findById(project.getId());
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.IN_PROGRESS.getDisplayName(), projectFind.getStatus()
        );
    }


    @Test
    public void testStartByIndex() {
        @NotNull final Project project = testProjectModel();
        @NotNull final IProjectRepository
                projectRepository = testRepository(project);
        project.setStatus(Status.IN_PROGRESS.getDisplayName());
        projectRepository.updateProjectQuery(project);
        sqlSession.commit();
        Assert.assertNotNull(project.getStatus());
        @Nullable final Project projectFind =
                projectRepository.findOneByIndex(project.getUserId(), 0);
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.IN_PROGRESS.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testStartByName() {
        @NotNull final Project project = testProjectModel();
        @NotNull final IProjectRepository
                projectRepository = testRepository(project);
        project.setStatus(Status.COMPLETE.getDisplayName());
        projectRepository.updateProjectQuery(project);
        sqlSession.commit();
        Assert.assertNotNull(project.getStatus());
        @Nullable final Project projectFind =
                projectRepository.findOneByName(
                        project.getUserId(), project.getName()
                );
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testUpdateOneById() {
        @NotNull final Project project = testProjectModel();
        @NotNull final IProjectRepository
                projectRepository = testRepository(project);
        project.setName("name2");
        project.setDescription("des2");
        projectRepository.updateProjectQuery(project);
        sqlSession.commit();
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        @Nullable final Project projectFind =
                projectRepository.findById(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        Assert.assertEquals("name2", projectFind.getName());
        Assert.assertEquals("des2", projectFind.getDescription());
    }


    @Test
    public void testUpdateOneByIndex() {
        @NotNull final Project project = testProjectModel();
        @NotNull final IProjectRepository
                projectRepository = testRepository(project);
        project.setName("name2");
        project.setDescription("des2");
        projectRepository.updateProjectQuery(project);
        sqlSession.commit();
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        @Nullable final Project projectFind =
                projectRepository.findOneByIndex(project.getUserId(), 0);
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        Assert.assertEquals("name2", projectFind.getName());
        Assert.assertEquals("des2", projectFind.getDescription());
    }

}
